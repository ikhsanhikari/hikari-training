package com.hikari.training.hikaritraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HikaritrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(HikaritrainingApplication.class, args);
	}

}
