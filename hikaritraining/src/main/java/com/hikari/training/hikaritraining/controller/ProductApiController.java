package com.hikari.training.hikaritraining.controller;

import com.hikari.training.hikaritraining.dao.ProductDao;
import com.hikari.training.hikaritraining.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController @RequestMapping("/api/product")
public class ProductApiController {
    @Autowired private ProductDao productDao;

    @GetMapping("/")
    public Page<Product> findProduct(Pageable pageable){
        return productDao.findAll(pageable);
    }

    @PostMapping("/save")
    public Product product(@RequestBody Product product){
        return productDao.save(product);
    }

}
