package com.hikari.training.hikaritraining.dao;

import com.hikari.training.hikaritraining.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product,String> {
}
