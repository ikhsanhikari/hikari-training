package com.hikari.training.hikaritraining.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
public class Product {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    private  String id;

    @NotNull @NotEmpty @Size(max = 100)
    private String name;

    @NotNull @NotEmpty
    private String code;
}
